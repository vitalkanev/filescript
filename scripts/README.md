# `scripts` directory

This directory includes the scripts used by VitalKanev quite sometimes.

## Structure

Name | What it does
-----|-------------
`install` | Post-installation scripts for Debians (Arch and so coming soon)
`misc` | Not categorized

## Executing script

1. `cd` to the directory (see the table above!)
2. If so, run:
```bash
chmod +x <script name>
```
3. Type the following and follow the on-screen instructions:
```bash
./<scriptname>
```

<hr/>

Anything like licence can be found in the repository root
