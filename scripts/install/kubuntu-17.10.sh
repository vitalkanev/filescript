#!/bin/bash

# An _experimental_ script for installing
# Kubuntu Artful
##########################################
# This will:
# * Install software (input included)
# * Add backports PPA (KDE 5.10 is old)
# * Ummm... All done?
##########################################
# -- VitalKanev
##########################################

# Check for root 

if test $EUID -ne 0; then
    echo "A root account is required to run"
    echo "this script (Just add 'sudo' before"
    echo "the script execution)"
    exit 1
fi

# Variables, just as requirement

thlocale=""
defaultapps="build-essential curl audacious vim vim-gtk3 zlib1g-dev httpie thunderbird thunderbird-locale-$thlocale hexchat htop gimp inkscape"
installselection=""
nodeinstall=""
beginupgrade=""

# Start installation - Add Official backports PPA

echo "*** Adding Backports PPA ***"
sudo add-apt-repository -y ppa:kubuntu-ppa/backports

# Ask user for a few(?) questions
echo "Do you want to install Node.js (Accepting 9 for latest and"
echo "l for LTS (8.x))?"
read -r nodeinstall

echo "What packages do you want to install optionaly with default ones? (l, package name separated by a space)"
read -l installselection

echo "Which Thunderbird language?"
echo "(This question is shown since Kubuntu does not include"
echo "Thunderbird by default)"
read -r thlocale

# OPTIONAL Node.js repo?

if test $nodeinstall -eq 9; then
    echo "Option accepted - Installing Node.js latest"
    wget -qO- https://deb.nodesource.com/setup_9.x | sudo -E bash -
elif test $nodeinstall -eq l; then
    echo "Option accepted - Installing Node.js LTS"
    wget -qO- https://deb.nodesource.com/setup_8.x | sudo -E bash -
else
    echo "Bypassing Node.js installation..."
fi

# For intresting thing ~
if test $installselection -eq l; then
    echo "List of default packages: $defaultapps"
    installselection=""
    read -r installselection
fi

# INSTALL!!!

if test $nodeinstall == [l9]; then
    installselection="$installselection nodejs"
fi

sudo apt update
sudo apt install -y $defaultapps $installselection

# After that, ask user to upgrade Plasma...

echo "Now, you can start a FULL uograde of your system - The KDE Plasma version is going"
echo "to be upgraded. Now, start 'sudo apt full-upgrade'? (This might take quite some time)"
read -r beginupgrade

if test $beginupgrade -eq y; then
    sudo apt full-upgrade
fi

# As it ends, print this message

cat << EOF
Thanks for using this script! You might want
to report issues at vitalkanev/filescript GitHub
repository!
EOF
