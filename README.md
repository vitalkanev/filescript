# `filescript`

Welcome to the `filescript`, where I collect installation
scripts and most of the config files!

To use one of these files:
- Clone the repository

```sh
    $ git clone https://github.com/vitalkanev/filescript.git
    # Easier with <hub.github.com> (Hub)
    $ hub clone vitalkanev/filescript
```

- Copy one of the files. You might need to change something
*outside* of the repository

```sh
    $ mv <file> ~
```

- For the shell scripts, you can run it right away from the
  repository folder

## License

This project uses the `MIT` (not to be confused with `X11` license).
Please visit the `LICENSE` file for more information

---

This project is powered by [Code of Conduct](./CODE_OF_CONDUCT.md)
